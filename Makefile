DESTDIR=~/.local/bin/

install:
	mkdir -p $(DESTDIR) 2> /dev/null

	cp sbb ~/.local/bin/
	chmod +x $(DESTDIR)sbb

uninstall:
	rm $(DESTDIR)sbb
