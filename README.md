# sbb

Web browser in posix shell

## Requirements
* [pup](https://github.com/ericchiang/pup)
* [curl](https://curl.se/)

### Install
```
curl https://codeberg.org/MrRawes/sbb/raw/branch/main/sbb -o .local/bin/sbb && chmod +x .local/bin/sbb 
```
